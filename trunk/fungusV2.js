var _config = {}
var _urlSRF = 'https://iot-srf.ais.co.th:14303'

var connectToPanty = (connection,callback) => {
	console.log('connect to pantry')
	var res = {
		status : '',
		header : '',
		body : ''
	}
	var xmlhttp = new XMLHttpRequest()
	var url = connection.url+connection.path
	console.log('Service URL => '+url)
	try{
		xmlhttp.open(connection.method,url,false)
		// xmlhttp.timeout = 10000
		// xmlhttp.responseType = 'json'
		for(var i in connection.header){
			xmlhttp.setRequestHeader(i,connection.header[i]) 
		}
		xmlhttp.onreadystatechange = function(){
			res.status = xmlhttp.status
			res.header = xmlhttp.getAllResponseHeaders()
			res.body = xmlhttp.responseText
			callback(res)
		};
		xmlhttp.ontimeout = function (e) {
			console.log('connection setTimeout')
			// throw new ServerAuthenErrorMessage(eServerAuthException.STATUS_CONNECTION_TIMEOUT422)
		}
		xmlhttp.send(connection.body)
		return res
	}catch(e){
		console.log(e)
		console.log('connection error')
		return res
		// throw new ServerAuthenErrorMessage(eServerAuthException.STATUS_CONNECTION_ERROR)
	}
}

//var loginCallback = (resSRF) => {
//	obtainAuthorityList(resSRF,(param)=>{
//		alert(param)
//	})
//}

var Login = (fileConfig,callback) => {
	console.log('------getWebview--------')
	_config = JSON.parse(getFile(fileConfig))
	console.log('config : '+JSON.stringify(_config))
	var connectionWebview = {
		method : 'GET',
		url : _urlSRF,
		path : '/'+encodeURIComponent(_config.clientId)+'?channel=webview',
		header : {},
		body : {}
	}
	console.log('data : '+JSON.stringify(connectionWebview))
	connectToPanty(connectionWebview,function(response){
		callback(response.body)
	})
}

var obtainAuthorityList = (Param,callback) => {
	console.log('------obtainAuthorityList--------')
	var obtainParam = JSON.parse(Param)
	console.log('data form login : '+JSON.stringify(obtainParam))

	var _ptsAppEnvironmentType=''

	if(_config.environment == '1'){
		_ptsAppEnvironmentType='production'
	}else if(_config.environment == '2'){
		_ptsAppEnvironmentType='partner_iot'
	}else if(_config.environment == '3'){
		_ptsAppEnvironmentType='internal_iot'
	}else if(_config.environment == '4'){
		_ptsAppEnvironmentType='development'
	}

	var reqBody = {
		xApp:obtainParam.xApp,
		authCode:obtainParam.authCode,
		redirectURL:_config.urlBackend,
		clientId:_config.clientId,
		platform:'Browser',
		ptsAppEnvironmentType:_ptsAppEnvironmentType,
		ol4:obtainParam.ol4,
		privateId:obtainParam.privateId
	}
	
	var connection = {
		method : 'POST',
		url : _config.urlRegister.substring(_config.urlRegister.indexOf('/'),0),
		path : _config.urlRegister.substring(_config.urlRegister.indexOf('/')),
		header : {},
		body : JSON.stringify(reqBody)
	}

	console.log('connection : '+JSON.stringify(connection))
	var _res = {}
	connectToPanty(connection,function(resHummus){
		console.log('Response form Hummus : '+resHummus)
		
		if(resHummus.status == 200){
			if(validationCommon.checkIsJSON(resHummus.body)){
				var body = JSON.parse(resHummus.body)
				console.log(body)
				if(validationCommon.checkIsNULL(body.developerMessage)||
				validationCommon.checkIsNULL(body.resultCode)||
				validationCommon.checkIsNULL(body.accessToken)||
				validationCommon.checkIsNULL(body.expireIn)||
				validationCommon.checkIsNULL(body.privateId)||
				validationCommon.checkIsNULL(body.ptsListOfAPI)||
				validationCommon.checkIsNULL(body.ptsWindowTime)||
				validationCommon.checkIsNULL(body.ol4)){
					// callback(body)
					_res = body
				}else{
					console.log('Missing Param')
					_res.developerMessage='System Error'
					_res.resultCode=90005
					_res.moreInfo=''
					// callback(_res)
				}
			}else{
				console.log('Not JSON')
				_res.developerMessage='Unknow format'
				_res.resultCode=90014
				_res.moreInfo=''
				// callback(_res)
			}
		}else{
			console.log('not 200')
			if(validationCommon.checkIsJSON(resHummus.body)){
				var body = JSON.parse(resHummus.body)
				if(validationCommon.checkIsNULL(body.developerMessage)||
				validationCommon.checkIsNULL(body.resultCode)){
					// callback(body)
				}else{
					_res.developerMessage='System Error'
					_res.resultCode=90005
					_res.moreInfo=''
					// callback(_res)
				}
			}else{
				_res.developerMessage='Unknow format'
				_res.resultCode=90014
				_res.moreInfo=''
				// callback(_res)
			}
		}
	})
	// console.log(_res)
	callback(_res)
}

var getFile = (path) => {
	var rawFile = new XMLHttpRequest()
	var data
    rawFile.open("GET",path,false)
    rawFile.onreadystatechange = () => {
        if(rawFile.readyState === 4 && rawFile.status === 200 || rawFile.status == 0){
            data = rawFile.responseText
        }else{
        	console.log("File not't found.")
        }
        
    }
    rawFile.send()
    return data
}

var validationCommon = {
	checkIsNULL : function(data){
		if(data == '' || data == undefined){
			return true
		}else{
			return false
		}
	},
	checkIsNotNULL : function(data){
		if(data == '' || data == undefined){
			return false
		}else{
			return true
		}
	},
	checkIsJSON : function(json){
		try{
			JSON.parse(json)
		}catch(e){
			return false
		}
		return true
	},
	chkDecodeBase64 : (str) => {
		try{
			atob(str)
		}catch(e){
			return false
		}
		return true
	}

}